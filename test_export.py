import pandas as pd
# import httpServer
import pandas as pd
import numpy as np
import open3d as o3d
import matplotlib.pyplot as plt
from httpServer import reconstruct_segmented_mesh
import time
import trimesh
from trimesh.proximity import ProximityQuery

from io import StringIO
import json

# create paths and load data
DEMOPATH = "scene.obj"

from pathlib import Path
txt = Path('data_full.txt').read_text()
reconstruct_segmented_mesh(txt, csv_path='test')


pass