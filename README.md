# HoloLabel: Mixed Reality Scene Annotation Tool on the HoloLens
![workflow](files/workflow.png)

### [Demo Video](https://www.youtube.com/watch?v=K0sVDIbSMh4)

## Installation
### Unity App
1. open `MixedRealityFeatureTool`
2. point project path to `AR-SceneAnnotation`
3. Features to get:
	- Mixed Reality Toolkit
		- Mixed Reality Toolkit Examples
		- Mixed Reality Toolkit Extensions
		- Mixed Reality Toolkit Foundation
		- Mixed Reality Toolkit Standard Assets
		- Mixed Reality Toolkit Tools
	- Platform Support
		- Mixed Reality OpenXR Plugin
		- Mixed Reality Scene Understanding
4. Open `Assets/Scenes/SampleScene.unity`
5. In Unity:
	5.1 `Mixed Reality -> Project -> Apply recommended project settings for HoloLens 2`
	5.2 `File -> Build Settings`: switch to `Universal Windows Platform`
	5.3 `Build`
6. Open `Build/AR-SceneAnnotation.sln` in Visual Studio
7. In Visual Studio:
	7.1 switch to `Release`, `ARM64`, `Device`
	7.2 `Start Without Debugging`

### Python Environment
Intall python environment using `requirements.txt`

## Running the App
If you just want to run the app, without the export & auto segment function, simply deploying the app as stated above is enough. Otherwise you must:
1. deploy the app
2. run `python httpServer.py`
3. within the HoloLens app, set the ip address to your local PC where you ran the python server from (this ip is also displayed by `httpServer.py`)
