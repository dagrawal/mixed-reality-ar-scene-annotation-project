# import httpServer
import pandas as pd
import numpy as np
import open3d as o3d
import matplotlib.pyplot as plt

import time
import trimesh
from trimesh.proximity import ProximityQuery

from io import StringIO
import json

# create paths and load data
DEMOPATH = "demo/scene.obj"

NUM_CLUSTERS = 30
DISTANCE_THRESHOLD = 0.1

debug = True

def string_labels_to_df(s):

    if len(s) == 0: # If no labels were not provided
        return None
        
    TESTDATA = StringIO(s)
    df = pd.read_csv(TESTDATA, sep=';|:|,', engine='python', header=None)
    return df


def preprocess_dataframe(df):
    
    if df is None: # If no labels were not provided
    	return None

    """
    :param df: pandas dataframe in the format in which it is returned from unity
    :return: cleaned dataframe containing only the points, labels and descriptions
    """
    df.columns = ['indexHeader', 'Index', 'positionHeader', 'x', 'y', 'z', 'labelHeader', 'Label', 'descriptionHeader',
                  'Description']
    df = df.replace(['\(', '\)'], ['', ''], regex=True)
    df.x = df.x.astype('float64')
    df.z = df.z.astype('float64')
    df.Label = df.Label.astype('string')
    df.Description = df.Description.astype('string')

    label_data = df.drop(labels=['indexHeader', 'positionHeader', 'labelHeader', 'descriptionHeader'], axis=1)
    return label_data


def segment_pointcloud(pcd: o3d.geometry.PointCloud, faces=None, distance_threshold=0.01, ransac_n=3, num_iterations=1000, num_clusters=NUM_CLUSTERS):
    """
    :param pcd: pointcloud to be segmented
    :return: dictionary of size (num_clusters), where each entry contains the indices of the faces (in the faces array)
            corresponding to the segment
    """
    segment_models = {}
    segments = {}
    segments_as_indices = {}

    max_plane_idx = num_clusters
    rest = pcd

    outliers = np.array([i for i in range(len(np.asarray(rest.points)))])  # everything
    segment_colors = np.zeros((max_plane_idx, 3))

    for i in range(max_plane_idx):
        number_of_points_to_segment = len(rest.points)

        if i < 20:
            colors = plt.get_cmap("tab20b")(i)
        elif i < 40:
            colors = plt.get_cmap("tab20c")(i)
        else:
            colors = plt.get_cmap("tab20")(i)


        # inliers_idx maps indices into "rest"-points (gives indices of points in "rest" that belong to a new cluster)
        segment_models[i], inliers_idx = rest.segment_plane(distance_threshold=distance_threshold, ransac_n=ransac_n, num_iterations=num_iterations)
        segments[i] = rest.select_by_index(inliers_idx)
        labels = np.array(segments[i].cluster_dbscan(eps=0.1, min_points=10))
        candidates = [len(np.where(labels == j)[0]) for j in np.unique(labels)]

        best_candidate = int(np.unique(labels)[np.where(candidates == np.max(candidates))[0]])

        segments[i] = segments[i].select_by_index(list(np.where(labels == best_candidate)[0]))


        ## Updated inliers and outliers indices
        inliers_idx = list(np.array(inliers_idx)[(labels == best_candidate)])
        outliers_idx = np.delete(np.array([i for i in range(number_of_points_to_segment)]), inliers_idx)

        rest = rest.select_by_index(inliers_idx, invert=True)

        ## Inliers and outliers in global indices
        inliers = outliers[inliers_idx]
        outliers = outliers[outliers_idx]

        # Mesh reconstruction information
        segments_as_indices[i] = inliers

        # color information
        segment_color = list(np.multiply(colors[:3], 255))
        segment_colors[i] = segment_color  # color that belongs to the cluster i

        segments[i].paint_uniform_color(list(colors[:3]))

        print("pass", i + 1, "/", max_plane_idx, "done.")

    # # Cluster the remaining outliers
    # labels = np.array(rest.cluster_dbscan(eps=0.05, min_points=5))
    # max_label = labels.max()
    # print(f"point cloud has {max_label + 1} clusters")
    #
    # colors = plt.get_cmap("tab10")(labels / (max_label if max_label > 0 else 1))
    # colors[labels < 0] = 0
    # rest.colors = o3d.utility.Vector3dVector(colors[:, :3])


    return segments_as_indices, segment_colors


def reconstruct_mesh(face_segments, point_face_matching, mesh, colors, visualize=True):
    """
    :param face_segments: segment indices into the [faces] array obtained when sampling pcd
    :param point_face_matching: faces array
    :param mesh: initial mesh
    :param colors: colors per segment

    :return Reconstructs the mesh painting the clusters in their corresponding colors and returns the segment
            (using majority voting) for each of the faces
    """
    # majority clustering

    num_clusters = len(face_segments)

    face_colors_majority_voting = np.zeros((len(mesh.faces), num_clusters))
    face_colors_segmented = np.zeros((num_clusters, len(mesh.faces), 3))

    # face_colors_segmented = np.zeros((len(mesh.faces), 3))
    for i in range(num_clusters):
        segment_i_points = point_face_matching[face_segments[i]]

        for point in segment_i_points:
            face_colors_majority_voting[point, i] += 1

        segment_i_faces = point_face_matching[segment_i_points]

        face_mask_i = np.zeros(len(point_face_matching), bool)
        face_mask_i[segment_i_faces] = 1

        # majority voting
        face_colors_majority_voting[segment_i_faces][i] += 1

    # find cluster for each triangle
    majority_cluster = np.argmax(face_colors_majority_voting, axis=1)

    # for each face, assign the color of the majority cluster
    face_colors = colors[majority_cluster]  # cluster per face
    if debug:
        face_colors[majority_cluster] = np.array([0, 0, 0])
    # visualize segmented mesh
    visuals = trimesh.visual.color.ColorVisuals(mesh=mesh, face_colors=face_colors)
    mesh.visual = visuals

    # store this for visualization
    if visualize:
        timestr = time.strftime("%Y%m%d_%H%M%S")
        trimesh.exchange.export.export_mesh(mesh, f'exported_{timestr}.obj')

    return majority_cluster
    # mesh.show()


def get_labelled_segments(mesh, label_data, clusterPerFace):
    """
    :param mesh: mesh
    :param label_data: label dataframe
    :param clusterPerFace: face segmentation
    :return: segment index for each of the labels
    """
    points = np.transpose(np.array([label_data['x'], label_data['y'], label_data['z']]))

    # closest faces to each of the label points
    closest_point, distance, closest_face = trimesh.proximity.closest_point(mesh, points)
    return clusterPerFace[closest_face]


def segment_and_generate_labels(mesh, label_data, store_segmented_mesh=False, distance_threshold=DISTANCE_THRESHOLD, ransac_n=3, num_iterations=1000):
    """
    :param mesh: trimesh-mesh
    :param label_data: pandas dataframe containing the label data
    :return: json containing a a list of length vertices with the corresponding cluster for each vertex, and a list of
        length  number of cluster which contains the index of the corresponding label (if provided)
    """

    num_clusters = len(mesh.vertices) // 800

    # transform mesh to pointcloud
    pointcloud, faces = trimesh.sample.sample_surface(mesh, 100000)  #  TODO: How many samples?

    # transform mesh to pointcloud for segmentation
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(pointcloud)

    # run segmentation on the pointcloud
    face_segments, colors = segment_pointcloud(pcd, faces, distance_threshold=distance_threshold, ransac_n=ransac_n, num_iterations=num_iterations, num_clusters=num_clusters)
    num_clusters = len(face_segments)
    # this is just for visualization purposes
    clusterPerFace = reconstruct_mesh(face_segments, faces, mesh, colors, visualize=store_segmented_mesh)

    if store_segmented_mesh:
        # store this for visualization
        timestr = time.strftime("%Y%m%d_%H%M%S")
        trimesh.exchange.export.export_mesh(mesh, f'exported_{timestr}.obj')

    # Segment Index to label mapping
    cluster_to_label = np.full(num_clusters, (-1))

    if label_data is not None: # Check if labels were provided.
    
        # segment index of the provided labels
        label_segments = get_labelled_segments(mesh, label_data, clusterPerFace)

        for i in range(len(label_segments)):
            seg = label_segments[i]

            if cluster_to_label[seg] == (-1):
                cluster_to_label[seg] = i
            else:
                cluster_to_label[seg] = -2  # -2 means error: Multiple labels for this segment

    vertex_to_cluster = np.full( len(mesh.vertices) , -1) #np.zeros((len(mesh.vertices)))

    # Given segments of faces, turn them into segments of vertices
    for segment_idx in face_segments:
        face_indices = face_segments[segment_idx]  # index in the faces array
        face = mesh.faces[faces[face_indices]]  # pointcloud points to actual faces
        vertex_to_cluster[face] = segment_idx

    # # Output separate parts
    # for segment_idx in face_segments:
    #     colors = mesh.visual.face_colors[:, :3]
    #     submesh = trimesh.base.Trimesh(vertices=mesh.vertices, faces=mesh.faces[faces[face_segments[segment_idx]]])
    #     visuals = trimesh.visual.color.ColorVisuals(mesh=submesh, face_colors=colors[faces[face_segments[segment_idx]]])
    #     submesh.visual = visuals
    #     trimesh.exchange.export.export_mesh(submesh,f"SegmentationQuality/clusters_{num_clusters}-threshold_{str(distance_threshold).replace('.', '')}-ransac_{ransac_n}-Segment_{segment_idx}.obj")

    dict = {"VertexSegment": vertex_to_cluster.astype(int).tolist(),
            "SegmentLabels": cluster_to_label.astype(int).tolist()}
    print("Done segmenting. Sending back segmentation results.")
    result = json.dumps(dict)

    return result


# Can be run directly for debugging using a prestored mesh with labels
def main():
    # Read the data
    df = pd.read_csv('demo/labels_full.txt', sep=';|:|,', engine='python', header=None)
    label_data = preprocess_dataframe(df)

    # load mesh data
    mesh = trimesh.load(DEMOPATH)
    trimesh.exchange.export.export_mesh(mesh, f'mesh_as_read.obj')
    # mesh.show()

    # for threshold in [0.1, 0.05, 0.01]:
    #     for num_clusters in [30, 40, 50, 60]:
    #         print(f"threshold: {threshold}, num_clusters: {num_clusters}")
    #         try:
    #             segment_and_generate_labels(mesh, label_data, store_segmented_mesh=False, distance_threshold=threshold)
    #             # store this for visualization
    #             trimesh.exchange.export.export_mesh(mesh, f"SegmentationQuality/clusters_{num_clusters}-threshold_{str(threshold).replace('.', '')}.obj")
    #
    #
    #         except:
    #             print("** Failed **")

if __name__ == '__main__':
    main()
