using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class SegmentHandler : MonoBehaviour
{
    public HololensMeshHandler mesh_handler;
    public createLabel create_label_script;
    public MeshPainter mesh_painter;
    public LayerMask layerMask;

    private enum State
    {
        PAINTING,
        IDLE
    }
    private State currentState;
    private float coolDownTime;

    // Start is called before the first frame update
    void Start()
    {
        currentState = State.IDLE;
        coolDownTime = 0.0f;
    }

    // Update is called once per frame
    public void Update()
    {
        if (create_label_script.mode == "draw_seg")
        {

            Vector3 startPoint = new Vector3(0.0f, 0.0f, 0.0f);
            Vector3 endPoint = new Vector3(0.0f, 0.0f, 0.0f);
            foreach (var source in CoreServices.InputSystem.DetectedInputSources)
            {
                if (source.SourceType == InputSourceType.Hand)
                {
                    foreach (var p in source.Pointers)
                    {
                        if (p is ShellHandRayPointer && p.Result != null)
                        {
                            startPoint = p.Position;
                            endPoint = p.Result.Details.Point;
                            var hitObject = p.Result.Details.Object;
                            if (hitObject)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            Mesh roomMesh = mesh_handler.scene_mesh_object.GetComponent<MeshFilter>().mesh;

            if (HandPoseUtils.CalculateIndexPinch(Handedness.Both) > 0.7f)
            {
                HololensConsole.DisplayMessage("Pinch Confirmed");

                if (currentState == State.IDLE)
                {
                    mesh_painter.UpdateColorHistory();
                    mesh_painter.UpdateMappingHistory();
                    currentState = State.PAINTING;
                }

                RaycastHit hit;
                bool didHit = Physics.Raycast(startPoint, endPoint - startPoint, out hit, Mathf.Infinity, layerMask);
                if (didHit)
                {
                    int p0 = roomMesh.triangles[hit.triangleIndex * 3 + 0];

                    int segmentIndex = mesh_handler.vertexSegmentIndices[p0];

                    if (segmentIndex == 0)
                    {
                        HololensConsole.DisplayMessage("Segment Index 0");
                        return;
                    }

                    Color c = mesh_painter.GetColor();
                    mesh_handler.segmentClassMapping[segmentIndex] = c;
                    UpdateColors(segmentIndex);
                }
            }
            else if (HandPoseUtils.CalculateIndexPinch(Handedness.Both) < 0.1f)
            {
                coolDownTime += Time.deltaTime;
                if (coolDownTime > 3.0)
                {
                    currentState = State.IDLE;
                    coolDownTime = 0.0f;
                }
            }
        }
    }

    public void UpdateColors(int segmentIndex)
    {
        HololensConsole.DisplayMessage("Udpating Colors");

        Mesh roomMesh = mesh_handler.scene_mesh_object.GetComponent<MeshFilter>().mesh;
        Color[] colors = roomMesh.colors;

        HololensConsole.DisplayMessage(segmentIndex.ToString());

        if (!mesh_handler.segmentVertexMapping.ContainsKey(segmentIndex))
        {
            HololensConsole.DisplayMessage("No Segment with this key");
        }

        List<int> segmentVertices = mesh_handler.segmentVertexMapping[segmentIndex];
        HololensConsole.DisplayMessage(segmentVertices.Count.ToString());

        foreach (int i in segmentVertices)
        {
            colors[i] = mesh_handler.segmentClassMapping[segmentIndex];
        }

        roomMesh.colors = colors;
    }
}
