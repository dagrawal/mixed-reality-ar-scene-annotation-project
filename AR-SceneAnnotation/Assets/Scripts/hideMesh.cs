using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.SpatialAwareness;
using Microsoft.MixedReality.Toolkit.XRSDK.OpenXR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hideMesh : MonoBehaviour
{
    public void meshHide()
    {
        // Get the first Mesh Observer available, generally we have only one registered
        var observer = CoreServices.GetSpatialAwarenessSystemDataProvider<OpenXRSpatialAwarenessMeshObserver>();

        // Set to not visible
        if (observer.DisplayOption != SpatialAwarenessMeshDisplayOptions.None)
        {
            observer.DisplayOption = SpatialAwarenessMeshDisplayOptions.None;
        } 
        else
        {
            observer.DisplayOption = SpatialAwarenessMeshDisplayOptions.Visible;
        }
        
    }
}
