using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.SpatialAwareness;
using Microsoft.MixedReality.Toolkit.XRSDK.OpenXR;
using System;
using System.Text;
using System.Linq;
using UnityEngine.Rendering;

public class MeshRepresentation
{
    public Vector3[] vertices;
    public int[] triangles;

    public MeshRepresentation(Vector3[] in_vertices, int[] in_triangles)
    {
        vertices = in_vertices;
        triangles = in_triangles;
    }

    public string SaveToString()
    {
        return JsonUtility.ToJson(this);
    }
}

public class MeshLabelRepresentation
{
    public Vector3[] vertices;
    public int[] triangles;
    public string label_data;

    public MeshLabelRepresentation(Vector3[] vertices, int[] triangles, string label_data)
    {
        this.vertices = vertices;
        this.triangles = triangles;
        this.label_data = label_data;
    }

    public string SaveToString()
    {
        return JsonUtility.ToJson(this);
    }
}

public class SegmentedMeshRepresentation
{
    public MeshRepresentation mesh; 

    public int[] vertexSegmentIndices;
    public Dictionary<int, Color> segmentClassMapping;
    public string descriptionString;

    public SegmentedMeshRepresentation(Vector3[] vertices, int[] triangles, int[] vertexSegmentIndices , Dictionary<int, Color> segmentClassMapping, string descriptionString)
    {
        this.mesh = new MeshRepresentation(vertices, triangles);
        this.vertexSegmentIndices = vertexSegmentIndices;
        this.segmentClassMapping = segmentClassMapping;
        this.descriptionString = descriptionString;
    }

    public string SaveToString()
    {
        string returnString = "{";

        returnString += "\"mesh\": "+ mesh.SaveToString()+",";
        returnString += "\"vertexSegmentIndices\": [" + string.Join(",",vertexSegmentIndices)+"],";

        var entries = segmentClassMapping.Select(d => string.Format("\"{0}\": {{ \"r\": {1}, \"g\": {2}, \"b\": {3} }}", d.Key,  d.Value.r, d.Value.g, d.Value.b));
            
        returnString += "\"segmentClassMapping\":{" + string.Join(",", entries) + "},";

        returnString += string.Format("\"descriptionString\": \"{0}\"" , descriptionString);
        
        returnString += "}";
        return returnString;
    }
}


public class ReturnValue
{
    public int[] VertexSegment;
    public int[] SegmentLabels;
}


public class HololensMeshHandler : MonoBehaviour
{
    public GameObject Dialog;
    public GameObject scene_mesh_object;
    public MeshPainter mesh_panter_reference;

    public Material Material;

    public List<int> vertexSegmentIndices;
    public Dictionary<int, Color> segmentClassMapping;
    public Dictionary<int , List<int>> segmentVertexMapping;

    // histories for undo button
    public LinkedList<List<int>> vertexSegmentIndicesHistory;
    public LinkedList<Dictionary<int, Color>> segmentClassMappingHitory;
    public LinkedList<Dictionary<int, List<int>>> segmentVertexMappingHistory;

    public void Start()
    {
        vertexSegmentIndices = new List<int>();
        segmentClassMapping = new Dictionary<int, Color>();
        segmentVertexMapping = new Dictionary<int, List<int>>();

        vertexSegmentIndicesHistory = new LinkedList<List<int>>();
        segmentClassMappingHitory = new LinkedList<Dictionary<int, Color>>();
        segmentVertexMappingHistory = new LinkedList<Dictionary<int, List<int>>>();
    }

    public void UpdateScene()
    {
        if (scene_mesh_object == null)
        {
            CreateSceneMesh();
        }
        else
        {
            UpdateUnityMesh();
        }
    }

    public void CreateSceneMesh()
    {

        // get mesh from Spatial Mapping
        MeshRepresentation m = GetMesh("hololens");
        Vector3[] vertices = m.vertices;
        int[] triangles = m.triangles;

        // create Unity mesh
        Mesh scene_mesh = new Mesh();
        scene_mesh.vertices = vertices;
        scene_mesh.triangles = triangles;

        Color[] colors = new Color[vertices.Length];

        for (int i = 0; i < vertices.Length; i++)
        {
            colors[i] = Color.white;
        }
        // assign the array of colors to the Mesh.
        scene_mesh.colors = colors;

        scene_mesh.RecalculateNormals();

        // create game object
        scene_mesh_object = new GameObject("Mesh", typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider));
        scene_mesh_object.GetComponent<MeshFilter>().mesh = scene_mesh;
        scene_mesh_object.GetComponent<MeshRenderer>().material = Material;
        scene_mesh_object.layer = 30;

        // add mesh collider
        scene_mesh.RecalculateBounds();
        scene_mesh_object.GetComponent<MeshCollider>().sharedMesh = scene_mesh;

        vertexSegmentIndices = (new int[vertices.Length]).ToList();
    }

    public MeshRepresentation GetUnityMesh(string cs = "classic")
    {
        MeshFilter scene_mf = scene_mesh_object.GetComponent<MeshFilter>();
        Mesh scene_mesh = scene_mf.mesh;
        if (cs == "classic")
        {
            Vector3[] vertices = scene_mesh.vertices;
            int[] triangles = scene_mesh.triangles;
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i].x = -vertices[i].x;
                vertices[i] = scene_mf.transform.InverseTransformPoint(vertices[i]);
            }
            for (int i = 0; i < triangles.Length; i += 3)
            {
                int tmp = triangles[i + 1];
                triangles[i + 1] = triangles[i + 0];
                triangles[i + 0] = tmp;
            }
            return new MeshRepresentation(vertices, triangles);
        }
        else
        {
            return new MeshRepresentation(scene_mesh.vertices, scene_mesh.triangles);
        }
    }

    public void ExportMeshDebug()
    {
        MeshFilter scene_mf = scene_mesh_object.GetComponent<MeshFilter>();
        Mesh scene_mesh = scene_mf.mesh;
        Vector3[] vertices = scene_mesh.vertices;
        int[] triangles = scene_mesh.triangles;
        Color[] colors = scene_mesh.colors;

        TextWriter sr = File.CreateText(Path.Combine(Application.persistentDataPath, "scene.obj"));
        StringBuilder sb_vertices = new StringBuilder();
        StringBuilder sb_faces = new StringBuilder();
        int vertexOffset = 0;
        
        // write vertices
        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 lv = vertices[i];
            Vector3 wv = scene_mf.transform.TransformPoint(lv);
            Color c = colors[i];

            sb_vertices.Append(string.Format(
                "v {0} {1} {2} {3} {4} {5}\n",
                floatToStr(-wv.x),
                floatToStr(wv.y),
                floatToStr(wv.z),
                floatToStr(c.r),
                floatToStr(c.g),
                floatToStr(c.b)
            ));
        }

        // write faces
        for (int i = 0; i < triangles.Length; i += 3)
        {
            // Because we inverted the x-component, we also needed to alter the triangle winding.
            sb_faces.Append(
                string.Format(
                    "f {1} {0} {2}\n",
                    triangles[i + 0] + 1 + vertexOffset,
                    triangles[i + 1] + 1 + vertexOffset,
                    triangles[i + 2] + 1 + vertexOffset
                )
            );
        }
        vertexOffset += vertices.Length;

        string mesh_string = sb_vertices.ToString() + sb_faces.ToString();
        sr.Write(mesh_string);
        sr.Close();
    }

    public void UpdateUnityMesh()
    {

        // get mesh from Spatial Mapping
        MeshRepresentation m = GetMesh("hololens");
        Vector3[] vertices = m.vertices;
        int[] triangles = m.triangles;

        // collect new, unseen triangles in here
        List<int> new_triangles_list = new List<int>();

        for (int i = 0; i < triangles.Length; i += 3)
        {
            // compute midpoint & normal of current triangle
            int p0 = triangles[i + 0];
            int p1 = triangles[i + 1];
            int p2 = triangles[i + 2];
            Vector3 v0 = vertices[p0];
            Vector3 v1 = vertices[p1];
            Vector3 v2 = vertices[p2];
            Vector3 A = v1 - v0;
            Vector3 B = v2 - v0;
            Vector3 n = Vector3.Cross(A, B).normalized;
            Vector3 mid_point = (v0 + v1 + v2) / 3.0f;

            // shoot ray back at the scene
            RaycastHit hit1;
            bool didHit1 = Physics.Raycast(mid_point + 0.05f * n, -n, out hit1, 0.2f, mesh_panter_reference.layerMask);

            // if the back-shooting ray hits nothing => no mesh there yet => the face is new
            if (!didHit1)
            {
                new_triangles_list.Add(p0);
                new_triangles_list.Add(p1);
                new_triangles_list.Add(p2);
            }
        }
        int[] tris = new_triangles_list.ToArray();

        // create scene mesh
        Mesh newpart_mesh = new Mesh();
        newpart_mesh.indexFormat = vertices.Length > 65536 ? IndexFormat.UInt32 : IndexFormat.UInt16;
        newpart_mesh.vertices = vertices;
        newpart_mesh.triangles = tris;
        newpart_mesh.Optimize();  // Note: this will take care of floating vertices
        newpart_mesh.RecalculateNormals();

        // get current unity mesh & create new one
        Mesh scene_mesh_curr = scene_mesh_object.GetComponent<MeshFilter>().mesh;
        Mesh scene_mesh = new Mesh();
        scene_mesh.indexFormat = IndexFormat.UInt32;

        // merge existing & new vertices
        List<Vector3> old_verts = scene_mesh_curr.vertices.ToList();
        int baseOffset = old_verts.Count;
        List<Vector3> new_verts = newpart_mesh.vertices.ToList();
        old_verts.AddRange(new_verts);
        Vector3[] merged_verts = old_verts.ToArray();
        scene_mesh.vertices = merged_verts;

        // add white as color for new vertices
        List<Color> old_colors = scene_mesh_curr.colors.ToList();
        for (int i = 0; i < new_verts.Count; i++)
        {
            old_colors.Add(Color.white);
        }
        Color[] merged_colors = old_colors.ToArray();
        scene_mesh.colors = merged_colors;

        // merge existing & new triangles
        List<int> old_triangles = scene_mesh_curr.triangles.ToList();
        List<int> new_triangles = newpart_mesh.triangles.ToList();
        for (int i = 0; i < new_triangles.Count; i++)
        {
            new_triangles[i] += baseOffset;
        }
        old_triangles.AddRange(new_triangles);
        int[] merged_triangles = old_triangles.ToArray();
        scene_mesh.triangles = merged_triangles;

        // meh optimizaton & normals
        //scene_mesh.Optimize();
        scene_mesh.RecalculateNormals();

        // delete current scene mesh
        Destroy(scene_mesh_object);
        
        // create game object
        scene_mesh_object = new GameObject("Mesh", typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider));
        scene_mesh_object.GetComponent<MeshFilter>().mesh = scene_mesh;
        scene_mesh_object.GetComponent<MeshRenderer>().material = Material;
        scene_mesh_object.layer = 30;

        // add mesh collider
        scene_mesh.RecalculateBounds();
        scene_mesh_object.GetComponent<MeshCollider>().sharedMesh = scene_mesh;

        // merge vertex segment ids
        List<int> new_vertexSegmentIndices = (new int[newpart_mesh.vertexCount]).ToList();
        vertexSegmentIndices.AddRange(new_vertexSegmentIndices);

    }

    public static MeshRepresentation GetMesh(string cs = "classic")
    {
        OpenXRSpatialAwarenessMeshObserver observer = CoreServices.GetSpatialAwarenessSystemDataProvider<OpenXRSpatialAwarenessMeshObserver>();
        IReadOnlyDictionary<int, SpatialAwarenessMeshObject> meshes = observer.Meshes;
        List<Vector3> sb_vertices = new List<Vector3>();
        List<int> sb_faces = new List<int>();
        int vertexOffset = 0;
        foreach (KeyValuePair<int, SpatialAwarenessMeshObject> kvp in meshes)
        {
            SpatialAwarenessMeshObject m = kvp.Value;
            MeshFilter mf = m.Filter;
            Mesh mesh = mf.mesh;
            Vector3[] vertices = mesh.vertices;
            int[] triangles = mesh.triangles;

            // write vertices
            foreach (Vector3 lv in vertices)
            {
                Vector3 wv = mf.transform.TransformPoint(lv);

                if (cs == "classic")
                {
                    // conversion to a a "normal" coordinate system
                    sb_vertices.Add(new Vector3(-wv.x, wv.y, wv.z));
                } else
                {
                    // keep hololens coordinate system
                    sb_vertices.Add(new Vector3(wv.x, wv.y, wv.z));
                }
                
            }

            // write faces
            for (int i = 0; i < triangles.Length; i += 3)
            {
                if (cs == "classic")
                {
                    // Because we inverted the x-component, we also needed to alter the triangle winding.
                    sb_faces.Add(triangles[i + 1] + vertexOffset);
                    sb_faces.Add(triangles[i + 0] + vertexOffset);
                    sb_faces.Add(triangles[i + 2] + vertexOffset);
                } else
                {
                    sb_faces.Add(triangles[i + 0] + vertexOffset);
                    sb_faces.Add(triangles[i + 1] + vertexOffset);
                    sb_faces.Add(triangles[i + 2] + vertexOffset);
                }
            }
            vertexOffset += vertices.Length;

        }
        return new MeshRepresentation(sb_vertices.ToArray() , sb_faces.ToArray());
    }

    public int GetClosestVertex(Vector3[] vertices , Vector3 position)
    {
        int index = 0;
        float minDist = float.MaxValue;
        for (int j = 0; j < vertices.Length; j++)
        {
            if (vertices[j] == null)
                continue;
            if ((vertices[j] - position).magnitude < minDist)
            {
                index = j;
                minDist = (vertices[j] - position).magnitude;
            }
        }
        return index;
    }

    public Dictionary<int, int> GetMeshProjectionVertices(Vector3[] new_vertices , Vector3[] old_vertices)
    {
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        try
        {
            Vector3 position;
            for (int i = 0; i < new_vertices.Length; i++)
            {
                if (new_vertices[i] == null)
                    continue;

                int index = 0;
                float minDist = float.MaxValue;
                position = new_vertices[i];
                for (int j = 0; j < old_vertices.Length; j++)
                {
                    if (old_vertices[j] == null)
                        continue;
                    if ((old_vertices[j] - position).magnitude < minDist)
                    {
                        index = j;
                        minDist = (old_vertices[j] - position).magnitude;
                    }
                }
                mapping.Add(i, index);
            }
            return mapping;
        }
        catch (Exception e)
        {
            string error = e.Message;
            GameObject g = Instantiate(Dialog, new Vector3(0f, 0f, 0.5f), Quaternion.identity);
            g.transform.GetChild(0).gameObject.SetActive(false);
            GameObject text = g.transform.GetChild(1).gameObject;
            text.SetActive(true);
            text.GetComponent<TextMeshPro>().text = error;
        }
        return mapping;
    }

    private static string floatToStr(float number)
    {
        return String.Format("{0:0.######}", number);
    }
}
