using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SegmentationHandler : MonoBehaviour
{

    public HololensMeshHandler mesh_handler;
    public GameObject CreateLabelScript;
    public int[] segmentPerVertex { get; private set; }
    public int[] labelPerSegment { get; private set; }

    private int segmentIndex = 1000;

    public void GetSegmentation()
    {
        HololensConsole.DisplayMessage("Beginning Auto Segmentation");
        createLabel labelCreator = CreateLabelScript.GetComponent<createLabel>();
        //MeshRepresentation currentMesh = HololensMeshHandler.GetMesh();

        Mesh m = mesh_handler.scene_mesh_object.GetComponent<MeshFilter>().mesh;
        MeshRepresentation currentMesh = mesh_handler.GetUnityMesh("classic");

        //Tuple<string, List<string>> labels = labelCreator.generateLabelString("full");
        MeshLabelRepresentation meshLabelRepresentation = new MeshLabelRepresentation(currentMesh.vertices, currentMesh.triangles, "");
        ReturnValue returnValue = HTTPClient.SendMeshAndWaitSegmentation(meshLabelRepresentation, "segmentation");

            
        if (returnValue == null)
        {            
            HololensConsole.DisplayMessage("Return Value is NULL");            
        }
           
        else if (returnValue.VertexSegment == null)            
        {           
            HololensConsole.DisplayMessage("VertexSegment is NULL");            
        }
            
        else if (returnValue.SegmentLabels == null)          
        {               
            HololensConsole.DisplayMessage("SegmentLabels is NULL");
        }

        segmentPerVertex = returnValue.VertexSegment;
        labelPerSegment = returnValue.SegmentLabels;

        HololensConsole.DisplayMessage(string.Format("The Number of Segments is {0}", labelPerSegment.Length));
        try
        {
            UpdateColors(segmentPerVertex, labelPerSegment);
        }
        catch (Exception e)
        {
            HololensConsole.DisplayMessage(e.Message);
        }
    }
    
    public void UpdateColors(int[] segmentPerVertex , int[] labelPerSegment)
    {
        Mesh currentMesh = mesh_handler.scene_mesh_object.GetComponent<MeshFilter>().mesh;
        Color[] colors = currentMesh.colors;
        int total_vertices = currentMesh.vertexCount;
        int currentSegmentIndex;

        HololensConsole.DisplayMessage(string.Format("Unity Mesh {0} Python Mesh {1} Unity vertices {2}" , total_vertices , segmentPerVertex.Length , mesh_handler.vertexSegmentIndices.Count));

        for(int i = 0; i < total_vertices; i++)
        {
            if (i > mesh_handler.vertexSegmentIndices.Count)
                break;

            if (i > segmentPerVertex.Length)
                break;

            //Check if already in a segment => Ignore if in a segment
            if(mesh_handler.vertexSegmentIndices[i] == 0)
            {
                if (segmentPerVertex[i] == -1) // Skip vertex if not assigned to any Segment (Its an outlier.)
                    continue;

                //Assign Segment to Vertex
                currentSegmentIndex = segmentPerVertex[i] + segmentIndex; // Offset for Painting Strokes. Update later.
                mesh_handler.vertexSegmentIndices[i] = currentSegmentIndex;

                //Add Vertex to Segment
                if (mesh_handler.segmentVertexMapping.ContainsKey(currentSegmentIndex))
                {
                    mesh_handler.segmentVertexMapping[currentSegmentIndex].Add(i);
                }
                else
                {
                    mesh_handler.segmentVertexMapping.Add(currentSegmentIndex , new List<int>());
                    mesh_handler.segmentVertexMapping[currentSegmentIndex].Add(i);
                }

                // Assign random color to new segment
                // Update to handle known class label
                if (!mesh_handler.segmentClassMapping.ContainsKey(currentSegmentIndex))
                {
                    Color classColor;
                    float r = UnityEngine.Random.Range(0.0f, 0.5f);
                    float g = UnityEngine.Random.Range(0.0f, 0.5f);
                    float b = UnityEngine.Random.Range(0.0f, 0.5f);
                    classColor = new Color(r, g, b , 0.5f);
                    
                    mesh_handler.segmentClassMapping.Add(currentSegmentIndex ,classColor);
                }

                if (i > colors.Length)
                    break;
                colors[i] = mesh_handler.segmentClassMapping[currentSegmentIndex];
            }
        }
        currentMesh.colors = colors;
        segmentIndex += labelPerSegment.Length; // Updating offset for calling automatic segmentation multiple times 

        //HololensConsole.DisplayMessage(string.Format("All {0} segments added", labelPerSegment.Length));
    }
}
