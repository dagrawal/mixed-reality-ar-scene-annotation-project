using UnityEngine;
using System.Net;
using System.IO;
using System.Text;

public class HTTPClient : MonoBehaviour
{

    public static string ip_string = "192.168.5.96";

    // send a mesh and label representation and wait for the segmentation result
    public static ReturnValue SendMeshAndWaitSegmentation(MeshLabelRepresentation data, string msg_type = "segmentation")
    {
        string postData = data.SaveToString();

        HttpWebResponse response = SendString(postData, msg_type);

        StreamReader reader = new StreamReader(response.GetResponseStream());

        string responseText = reader.ReadToEnd();
        Debug.Log("responseText: " + responseText);

        ReturnValue returnObj = JsonUtility.FromJson<ReturnValue>(responseText);

        HololensConsole.DisplayMessage("Created Return OBJ");

        //Debug.Log("VertexSegment " + returnObj.VertexSegment[0]);
        //Debug.Log("SegmentLabels " + returnObj.SegmentLabels[0]);

        //response.Close();

        return returnObj;
    }

    public static void SendMesh(SegmentedMeshRepresentation mesh, string msg_type)
    {
        string postData = mesh.SaveToString();
        SendString(postData, msg_type);
    }

    public static HttpWebResponse SendString(string postData, string msg_type)
    {
        HololensConsole.DisplayMessage("http://" + ip_string + ":5000/upload");
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(
              "http://" + ip_string + ":5000/upload");

        request.Method = WebRequestMethods.Http.Post;
        request.ContentType = "application/x-www-form-urlencoded";
        request.Headers["Message-Type"] = msg_type;

        byte[] byteArray = Encoding.UTF8.GetBytes(postData);

        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.

        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();

        // Get the response.

        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        Debug.Log(response);

        return response;
    }
}
