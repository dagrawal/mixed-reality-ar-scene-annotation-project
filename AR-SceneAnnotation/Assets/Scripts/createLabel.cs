using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.SpatialAwareness;
using Microsoft.MixedReality.Toolkit.XRSDK.OpenXR;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using System;
using System.Text;

public class createLabel : MonoBehaviour
{
    public GameObject template_label;
    public GameObject template_label_head;
    public GameObject template_label_manual;

    public GameObject hand_menu;

    public List<GameObject> labels = new List<GameObject>();

    public GameObject label_list;

    public GameObject annot_menu;

    public GameObject current_label = null;

    public Material plane_material;

    public GameObject dictator_panel;

    public HololensMeshHandler mesh_handler;

    public string mode;
    public Material active_mat;
    public Material inactive_mat;
    public GameObject label_mode_plate;
    public GameObject draw_face_mode_plate;
    public GameObject draw_seg_mode_plate;
    public GameObject active_label_plate;

    UnityEngine.TouchScreenKeyboard keyboard;
    public static string keyboardText = "";
    public TextMeshProUGUI ip_text_field;

    private enum LastAction
    {
        NONE,
        CREATE,
        PLACE,
        DESCRIBE
    }

    private LastAction currentLastAction; 

    private Vector3 lastPosition ;
    private Quaternion lastRotation ;
    private string last_verbal_text;

    void Start()
    {
        annot_menu.SetActive(false);
        mode = "label";
        currentLastAction = LastAction.NONE;
        SetUndoButtonActive(false);

    }

    // Update is called once per frame
    public void Update()
    {
        if (keyboard.status == TouchScreenKeyboard.Status.Visible)
        {
            keyboardText = keyboard.text;
            ip_text_field.text = keyboardText;
            HTTPClient.ip_string = keyboardText;
        }
    }

    public void OpenKeyboard()
    {
        keyboard = TouchScreenKeyboard.Open(HTTPClient.ip_string, TouchScreenKeyboardType.NumberPad);
    }

    public void SetUndoButtonActive(bool toggle)
    {
        Color c = Color.gray;
        if (toggle)
        {
            c = Color.white;
        }
        GameObject button_text = GameObject.FindGameObjectsWithTag("UndoButtonText")[0];
        button_text.GetComponent<TextMeshPro>().color = c;
        GameObject[] button_icon = GameObject.FindGameObjectsWithTag("UndoButtonIcon");
        for (int i = 0; i < button_icon.Length; i++)
        {
            button_icon[i].GetComponent<Renderer>().material.color = c;
        }
    }

    public void SetMode(string newMode)
    {
        HololensConsole.DisplayMessage("The New Mode is " + newMode);

        label_mode_plate.GetComponent<MeshRenderer>().material = inactive_mat;
        draw_face_mode_plate.GetComponent<MeshRenderer>().material = inactive_mat;
        draw_seg_mode_plate.GetComponent<MeshRenderer>().material = inactive_mat;
        switch (newMode)
        {
            case "label":
                label_mode_plate.GetComponent<MeshRenderer>().material = active_mat;
                SetLabelPlacementActive(true);
                annot_menu.SetActive(true);
                mode = newMode;
                break;
            case "draw_face":
                draw_face_mode_plate.GetComponent<MeshRenderer>().material = active_mat;
                SetLabelPlacementActive(false);
                annot_menu.SetActive(true);
                mode = newMode;
                break;
            case "draw_seg":
                draw_seg_mode_plate.GetComponent<MeshRenderer>().material = active_mat;
                SetLabelPlacementActive(false);
                annot_menu.SetActive(true);
                mode = newMode;
                break;
            default:
                Debug.Log("Unknown MODE");
                break;
        }
    }

    public void SetLabelButtonMat(GameObject plate)
    {
        active_label_plate.GetComponent<MeshRenderer>().material = inactive_mat;
        active_label_plate = plate;
        active_label_plate.GetComponent<MeshRenderer>().material = active_mat;
    }

    public void SetLabelPlacementActive(bool activeState)
    {
        GameObject[] labels = GameObject.FindGameObjectsWithTag("Label");

        foreach (GameObject label in labels)
        {
            ObjectManipulator manual = label.GetComponent<ObjectManipulator>();

            if(manual != null)
            {
                manual.enabled = activeState;
            }

            TapToPlace tapToPlace = label.GetComponent<TapToPlace>();

            if(tapToPlace != null)
            {
                tapToPlace.enabled = activeState;
            }
        }
    }

    public void selectCurrent(GameObject button)
    {
        current_label = button.transform.parent.gameObject;
        currentLastAction = LastAction.NONE;
    }

    public void toggleMenu()
    {     

        // Set to not visible
        if (annot_menu.activeSelf)
        {
            annot_menu.SetActive(false);
        }
        else
        {
            annot_menu.SetActive(true);
        }

    }

    public void assignDescription()
    {

        currentLastAction = LastAction.DESCRIBE;
        SetUndoButtonActive(true);

        // extract verbal label description
        GameObject tc = dictator_panel.transform.Find("TextContent").gameObject;
        GameObject tt = tc.transform.Find("TranscribedText").gameObject;
        string verbal_text = tt.GetComponent<TextMeshProUGUI>().text;

        // assign label text
        GameObject quad = current_label.transform.Find("Quad").gameObject;
        GameObject canvas = quad.transform.Find("Canvas").gameObject;
        GameObject textfield = canvas.transform.Find("DescriptionDisplayText").gameObject;
        last_verbal_text = textfield.GetComponent<Text>().text;
        textfield.GetComponent<Text>().text = verbal_text;
    }

    private static string floatToStr(float number)
    {
        return String.Format("{0:0.######}", number);
    }

    public void setLabelDisplayText(string category)
    {
        if (current_label == null)
        {
            return;
        }
        if (mode == "label")
        {
            Transform trans = current_label.transform;
            foreach (Transform c in trans.GetComponentsInChildren<Transform>())
            {
                if (c.gameObject.name == "LabelDisplayText")
                {
                    Text child = c.gameObject.GetComponent<Text>();
                    child.text = category;
                }

                if (c.gameObject.name == "Quad")
                {
                    GameObject plane = c.gameObject.gameObject;
                    plane.GetComponent<MeshRenderer>().material = plane_material;
                }
            }
        }
    }

    #region Networking Labels Export
    public void exportLabels()
    {

        // mesh
        //MeshRepresentation currentMesh = HololensMeshHandler.GetMesh();
        MeshRepresentation currentMesh = mesh_handler.GetUnityMesh("classic");
        
        // annotations
        StringBuilder sr_full = new StringBuilder();
        StringBuilder sr_xyz = new StringBuilder();

        for(var i = 0; i < labels.Count; i++)
        {
            GameObject label = labels[i];

            // extract label position
            GameObject label_cylinder = label.transform.Find("Cylinder").gameObject;
            Vector3 label_position = label_cylinder.transform.position;
            var x = -label_position[0];
            var y = label_position[1];
            var z = label_position[2];

            // extract label text
            GameObject quad = label.transform.Find("Quad").gameObject;
            GameObject canvas = quad.transform.Find("Canvas").gameObject;
            //GameObject textfield = canvas.transform.Find("LabelDisplayText").gameObject;
            //string label_text = textfield.GetComponent<Text>().text;

            // extract verbal label description
            GameObject textfield_verbal = canvas.transform.Find("DescriptionDisplayText").gameObject;
            string verbal_text = textfield_verbal.GetComponent<Text>().text;


            // write everything to disk
            sr_full.Append(String.Format("Index: {0}; Position: ({1},{2},{3}); Description: {4}\n", i, x, y, z, verbal_text));
            sr_xyz.Append(String.Format("{0};{1};{2}\n", x, y, z));
        }

        HTTPClient.SendString(sr_full.ToString(), "fulllabelexport");
        HTTPClient.SendString(sr_xyz.ToString(), "xyzlabelexport");

        SegmentedMeshRepresentation segmentedMesh = new SegmentedMeshRepresentation(currentMesh.vertices, currentMesh.triangles, mesh_handler.vertexSegmentIndices.ToArray(), mesh_handler.segmentClassMapping , sr_full.ToString());
        HTTPClient.SendMesh(segmentedMesh, "meshexport");
    }

    public void exportLabelsLocally()
    {
        // mesh (adapted from https://gist.github.com/NewEXE/daaef44c04f11d1e8c1e3d68cd64f82f)

        OpenXRSpatialAwarenessMeshObserver observer = CoreServices.GetSpatialAwarenessSystemDataProvider<OpenXRSpatialAwarenessMeshObserver>();
        IReadOnlyDictionary<int, SpatialAwarenessMeshObject> meshes = observer.Meshes;
        TextWriter sr = File.CreateText(Path.Combine(Application.persistentDataPath, "scene.obj"));
        StringBuilder sb_vertices = new StringBuilder();
        StringBuilder sb_faces = new StringBuilder();
        int vertexOffset = 0;
        foreach (KeyValuePair<int, SpatialAwarenessMeshObject> kvp in meshes)
        {
            SpatialAwarenessMeshObject m = kvp.Value;
            MeshFilter mf = m.Filter;
            Mesh mesh = mf.mesh;
            Vector3[] vertices = mesh.vertices;
            int[] triangles = mesh.triangles;

            //sr.WriteLine("mf name = {0}", mf.name);

            // write vertices
            foreach (Vector3 lv in vertices)
            {
                Vector3 wv = mf.transform.TransformPoint(lv);

                // This is sort of ugly - inverting x-component since we're in
                // a different coordinate system than "everyone" is "used to".
                sb_vertices.Append(string.Format(
                    "v {0} {1} {2}\n",
                    floatToStr(-wv.x),
                    floatToStr(wv.y),
                    floatToStr(wv.z)
                ));
            }

            // write faces
            for (int i = 0; i < triangles.Length; i += 3)
            {
                // Because we inverted the x-component, we also needed to alter the triangle winding.
                sb_faces.Append(
                    string.Format(
                        "f {1} {0} {2}\n",
                        triangles[i + 0] + 1 + vertexOffset,
                        triangles[i + 1] + 1 + vertexOffset,
                        triangles[i + 2] + 1 + vertexOffset
                    )
                );
            }
            vertexOffset += vertices.Length;
            
        }
        string mesh_string = sb_vertices.ToString() + sb_faces.ToString();
        sr.Write(mesh_string);
        sr.Close();



        // labels

        string path_full = Path.Combine(Application.persistentDataPath, "labelExport.txt");
        string path_xyz = Path.Combine(Application.persistentDataPath, "pointOnlyExport.txt");
        TextWriter sr_full = File.CreateText(path_full);
        TextWriter sr_xyz = File.CreateText(path_xyz);

        for (var i = 0; i < labels.Count; i++)
        {
            GameObject label = labels[i];

            // extract label position
            GameObject label_cylinder = label.transform.Find("Cylinder").gameObject;
            Vector3 label_position = label_cylinder.transform.position;
            var x = -label_position[0];
            var y = label_position[1];
            var z = label_position[2];

            // extract label text
            GameObject quad = label.transform.Find("Quad").gameObject;
            GameObject canvas = quad.transform.Find("Canvas").gameObject;
            //GameObject textfield = canvas.transform.Find("LabelDisplayText").gameObject;
            //string label_text = textfield.GetComponent<Text>().text;

            // extract verbal label description
            GameObject textfield_verbal = canvas.transform.Find("DescriptionDisplayText").gameObject;
            string verbal_text = textfield_verbal.GetComponent<Text>().text;


            // write everything to disk
            sr_full.WriteLine("Index: {0}; Position: ({1},{2},{3});  Description: {4}", i, x, y, z,verbal_text);
            sr_xyz.WriteLine("{0};{1};{2}", x, y, z);
        }

        sr_full.Close();
        sr_xyz.Close();

    }

    public Tuple<string , List<string>> generateLabelString(string labelType = "full")
    {
        StringBuilder sr_full = new StringBuilder();
        StringBuilder sr_xyz = new StringBuilder();

        List<string> labelsClasses = new List<string>() ;

        for (var i = 0; i < labels.Count; i++)
        {
            GameObject label = labels[i];

            // extract label position
            GameObject label_cylinder = label.transform.Find("Cylinder").gameObject;
            Vector3 label_position = label_cylinder.transform.position;
            var x = -label_position[0];
            var y = label_position[1];
            var z = label_position[2];

            // extract label text
            GameObject quad = label.transform.Find("Quad").gameObject;
            GameObject canvas = quad.transform.Find("Canvas").gameObject;
            //GameObject textfield = canvas.transform.Find("LabelDisplayText").gameObject;
            //string label_text = textfield.GetComponent<Text>().text;
            //labelsClasses.Add(label_text);
            // extract verbal label description
            GameObject textfield_verbal = canvas.transform.Find("DescriptionDisplayText").gameObject;
            string verbal_text = textfield_verbal.GetComponent<Text>().text;

            // write everything to disk
            sr_full.Append(String.Format("Index: {0}; Position: ({1},{2},{3}); Description: {4}\n", i, x, y, z,  verbal_text));
            sr_xyz.Append(String.Format("{0};{1};{2}\n", x, y, z));
        }

        if (labelType == "xyz")
        {
            return new Tuple<string , List<string>>(sr_xyz.ToString() , labelsClasses);
        }
        else
        {
            return new Tuple<string, List<string>>(sr_full.ToString(), labelsClasses);
        }
    }

    #endregion

    #region Create Label GameObjects
    public void labelCreate()
    {
        if (mode != "label")
            return;
        annot_menu.SetActive(true);

        // clone label
        GameObject new_label = GameObject.Instantiate(template_label);
        new_label.tag = template_label.tag;

        // set label oriantation & position
        new_label.transform.position = new Vector3(
            hand_menu.transform.position.x,
            hand_menu.transform.position.y + 0.15F,
            hand_menu.transform.position.z);
        new_label.transform.rotation = hand_menu.transform.rotation;

        // label_list.transform.position = new Vector3(
        //     hand_menu.transform.position.x,
        //     hand_menu.transform.position.y - 0.2F,
        //     hand_menu.transform.position.z);
        /*label_list.transform.position = hand_menu.transform.position + 
                                        Mathf.Cos(hand_menu.transform.eulerAngles.y * Mathf.Deg2Rad)*transform.right/5 -
                                        Mathf.Sin(hand_menu.transform.eulerAngles.y * Mathf.Deg2Rad)*transform.forward/5 ;
        label_list.transform.rotation = hand_menu.transform.rotation;*/

        // add label to list of labels
        labels.Add(new_label);
        current_label = new_label;
        currentLastAction = LastAction.CREATE;
        SetUndoButtonActive(true);
    }

    public void labelCreateHead()
    {

        if (mode != "label")
            return;
        annot_menu.SetActive(true);

        // clone label
        GameObject new_label = GameObject.Instantiate(template_label_head);
        new_label.tag = template_label.tag;

        // set label oriantation & position
        new_label.transform.position = new Vector3(
            hand_menu.transform.position.x,
            hand_menu.transform.position.y + 0.15F,
            hand_menu.transform.position.z);
        new_label.transform.rotation = hand_menu.transform.rotation;

        // label_list.transform.position = new Vector3(
        //     hand_menu.transform.position.x,
        //     hand_menu.transform.position.y - 0.2F,
        //     hand_menu.transform.position.z);
        /*label_list.transform.position = hand_menu.transform.position + 
                                        Mathf.Cos(hand_menu.transform.eulerAngles.y * Mathf.Deg2Rad)*transform.right/5 -
                                        Mathf.Sin(hand_menu.transform.eulerAngles.y * Mathf.Deg2Rad)*transform.forward/5 ;
        label_list.transform.rotation = hand_menu.transform.rotation;*/

        // add label to list of labels
        labels.Add(new_label);
        current_label = new_label;
        currentLastAction = LastAction.CREATE;
        SetUndoButtonActive(true);
    }

    public void labelCreateManual()
    {

        if (mode != "label")
            return;

        annot_menu.SetActive(true);

        // clone label
        GameObject new_label = GameObject.Instantiate(template_label_manual);
        new_label.tag = template_label.tag;

        // set label oriantation & position
        new_label.transform.position = new Vector3(
            hand_menu.transform.position.x,
            hand_menu.transform.position.y + 0.15F,
            hand_menu.transform.position.z);
        new_label.transform.rotation = hand_menu.transform.rotation;
        
        // label_list.transform.position = new Vector3(
        //     hand_menu.transform.position.x,
        //     hand_menu.transform.position.y - 0.2F,
        //     hand_menu.transform.position.z);
        /*label_list.transform.position = hand_menu.transform.position + 
                                        Mathf.Cos(hand_menu.transform.eulerAngles.y * Mathf.Deg2Rad)*transform.right/5 -
                                        Mathf.Sin(hand_menu.transform.eulerAngles.y * Mathf.Deg2Rad)*transform.forward/5 ;
        label_list.transform.rotation = hand_menu.transform.rotation;*/

        // add label to list of labels
        labels.Add(new_label);
        current_label = new_label;
        currentLastAction = LastAction.CREATE;
        SetUndoButtonActive(true);
    }

    #endregion

    #region UndoButton
    public void updateLastTransform()
    {
        HololensConsole.DisplayMessage("Started Placing Label named " + current_label.name);

        lastPosition = current_label.transform.position;
        lastRotation = current_label.transform.rotation;
        currentLastAction = LastAction.PLACE;
        SetUndoButtonActive(true);
    }

    public void undoLastAction()
    {
        if (mode != "label")
        {
            return;
        }

        HololensConsole.DisplayMessage("The current Last Action is " + currentLastAction.ToString());

        if(currentLastAction == LastAction.CREATE)
        {
            labels.RemoveAt(labels.Count - 1);
            Destroy(current_label);
            current_label = null;
        }
        else if (currentLastAction == LastAction.PLACE) 
        {
            current_label.transform.position = lastPosition;
            current_label.transform.rotation = lastRotation;
        }
        else if (currentLastAction == LastAction.DESCRIBE) // STILL NEED TO TEST
        {
            if(current_label != null)
            {
                GameObject quad = current_label.transform.Find("Quad").gameObject;
                GameObject canvas = quad.transform.Find("Canvas").gameObject;
                GameObject textfield = canvas.transform.Find("DescriptionDisplayText").gameObject;
                textfield.GetComponent<Text>().text = last_verbal_text;
                last_verbal_text = "No Description";
            }
        }

        currentLastAction = LastAction.NONE;
        SetUndoButtonActive(false);
    }

    #endregion
}
