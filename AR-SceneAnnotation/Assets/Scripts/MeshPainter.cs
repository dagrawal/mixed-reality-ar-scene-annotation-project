using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class MeshPainter : MonoBehaviour
{
    public enum SupportedColors
    {
        RED,
        GREY,
        GREEN,
        BLUE,
        LIGHTBLUE,
        LIGHTGREEN,
        YELLOW,
        PINK,
        PURPLE,
        BROWN,
        DARKGREEN,
        BLACK
    }
    
    private enum State
    {
        PAINTING,
        IDLE
    }

    // Set in the Editor
    public HololensMeshHandler mesh_handler;
    public createLabel create_label_script;
    public GameObject colorPallateList;
    public LayerMask layerMask;

    public SupportedColors currentColor = SupportedColors.RED;
    
    private List<int> selectedTriangles;
    private LinkedList<Color[]> colorHistory; //Stores the last 5 strokes.

    private State currentState;
    private int numberOfStrokes;

    private float coolDownTime;

    // Start is called before the first frame update
    void Start()
    {
        selectedTriangles = new List<int>();
        currentState = State.IDLE;
        numberOfStrokes = 0;
        coolDownTime = 0.0f;
        colorHistory = new LinkedList<Color[]>();
    }

    // Update is called once per frame
    void Update()
    {

        if (create_label_script.mode == "draw_face")
        {

            Vector3 startPoint = new Vector3(0.0f, 0.0f, 0.0f);
            Vector3 endPoint = new Vector3(0.0f, 0.0f, 0.0f);
            foreach (var source in CoreServices.InputSystem.DetectedInputSources)
            {
                if (source.SourceType == InputSourceType.Hand)
                {
                    foreach (var p in source.Pointers)
                    {
                        if (p is ShellHandRayPointer && p.Result != null)
                        {
                            startPoint = p.Position;
                            endPoint = p.Result.Details.Point;
                            var hitObject = p.Result.Details.Object;
                            if (hitObject)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            Mesh roomMesh = mesh_handler.scene_mesh_object.GetComponent<MeshFilter>().mesh;
            Color[] colors = roomMesh.colors;
            //Debug.Log(HandPoseUtils.CalculateIndexPinch(Handedness.Both));

            if (HandPoseUtils.CalculateIndexPinch(Handedness.Both) > 0.7f)
            {

                if(currentState == State.IDLE)
                {
                    UpdateColorHistory();
                    UpdateMappingHistory();
                    currentState = State.PAINTING;
                    numberOfStrokes++;
                    HololensConsole.DisplayMessage("Total number of strokes is " + numberOfStrokes.ToString());
                }

                RaycastHit hit;
                bool didHit = Physics.Raycast(startPoint, endPoint - startPoint, out hit, Mathf.Infinity, layerMask);
                if (didHit)
                {
                    int p0 = roomMesh.triangles[hit.triangleIndex * 3 + 0];
                    int p1 = roomMesh.triangles[hit.triangleIndex * 3 + 1];
                    int p2 = roomMesh.triangles[hit.triangleIndex * 3 + 2];

                    Color c = GetColor();
                    colors[p0] = c;
                    colors[p1] = c;
                    colors[p2] = c;

                    if(mesh_handler.vertexSegmentIndices[p0] != 0)
                    {
                        //Remove from old segment
                        List<int> old_segment = mesh_handler.segmentVertexMapping[mesh_handler.vertexSegmentIndices[p0]];
                        old_segment.Remove(p0);
                    }

                    if (mesh_handler.vertexSegmentIndices[p1] != 0)
                    {
                        //Remove from old segment
                        List<int> old_segment = mesh_handler.segmentVertexMapping[mesh_handler.vertexSegmentIndices[p0]];
                        old_segment.Remove(p1);
                    }

                    if (mesh_handler.vertexSegmentIndices[p2] != 0)
                    {
                        //Remove from old segment
                        List<int> old_segment = mesh_handler.segmentVertexMapping[mesh_handler.vertexSegmentIndices[p0]];
                        old_segment.Remove(p2);
                    }

                    mesh_handler.vertexSegmentIndices[p0] = numberOfStrokes;
                    mesh_handler.vertexSegmentIndices[p1] = numberOfStrokes;
                    mesh_handler.vertexSegmentIndices[p2] = numberOfStrokes;
                    
                    if (!mesh_handler.segmentClassMapping.ContainsKey(numberOfStrokes))
                    {
                        mesh_handler.segmentClassMapping.Add(numberOfStrokes , c);
                        mesh_handler.segmentVertexMapping.Add(numberOfStrokes , new List<int>());
                    }

                    mesh_handler.segmentVertexMapping[numberOfStrokes].Add(p0);
                    mesh_handler.segmentVertexMapping[numberOfStrokes].Add(p1);
                    mesh_handler.segmentVertexMapping[numberOfStrokes].Add(p2);

                    this.selectedTriangles.Add(hit.triangleIndex);
                }
            }
            else if(HandPoseUtils.CalculateIndexPinch(Handedness.Both) < 0.1f)
            {
                coolDownTime += Time.deltaTime;

                if(coolDownTime > 3.0)
                {
                    currentState = State.IDLE;
                    coolDownTime = 0.0f;
                }
            }
            roomMesh.colors = colors;
        }
    }

    public static Color ConvertClassToColor(string className)
    {
        Color classColor = new Color(0.0f,0.0f,0.0f);
        switch (className)
        {
            case "FLOOR":
                classColor = Color.blue;
                break;
            case "CEILING":
                classColor = Color.red;
                break;
            case "WALL":
                classColor = new Color(0.1f, 0.5f, 0.0f);
                break;
            case "OTHER":
                classColor = Color.black;
                break;
            
            case "COUCH":
                classColor = new Color(0.0f, 1.0f, 1.0f);
                break;
            case "CHAIR":
                classColor = new Color(0.0f, 1.0f, 0.0f);
                break;
            case "TABLE":
                classColor = new Color(1.0f, 0.7f, 0.0f);
                break;
            case "COUNTER":
                classColor = Color.magenta;
                break;

            case "REFRIGERATOR":
                classColor = new Color(0.5f, 0.0f, 0.5f);
                break;
            case "OVEN":
                classColor = new Color(0.5f, 0.4f, 0.0f);
                break;
            case "CABINET":
                classColor = new Color(0.0f, 0.5f, 0.4f);
                break;
            case "UNLABELLED":
                classColor = Color.white;
                break;
        }

        return classColor;
    }

    public Color GetColor()
    {
        switch (currentColor)
        {
            case SupportedColors.BLUE:
                return Color.blue;
            case SupportedColors.RED:
                return Color.red;
            case SupportedColors.GREEN:
                return new Color(0.1f, 0.5f, 0.0f);
            case SupportedColors.BLACK:
                return Color.black;

            case SupportedColors.LIGHTBLUE:
                return new Color(0.0f, 1.0f, 1.0f);
            case SupportedColors.LIGHTGREEN:
                return new Color(0.0f, 1.0f, 0.0f);
            case SupportedColors.YELLOW:
                return new Color(1.0f, 0.7f, 0.0f);
            case SupportedColors.PINK:
                return Color.magenta;
            
            case SupportedColors.PURPLE:
                return new Color(0.5f, 0.0f, 0.5f);
            case SupportedColors.BROWN:
                return new Color(0.5f, 0.4f, 0.0f);
            case SupportedColors.DARKGREEN:
                return new Color(0.0f, 0.5f, 0.4f);
            case SupportedColors.GREY:
                return Color.white;
        }
        return Color.gray;
    }
    public void ToggleColorPallateList()
    {
        colorPallateList.SetActive(!colorPallateList.activeSelf);
    }

    public void SetCurrentColor(string newColor)
    {
        if (newColor == "RED")
        {
            currentColor = SupportedColors.RED;
        }   
        else if (newColor == "GREY")
        {
            currentColor = SupportedColors.GREY;
        }
        else if (newColor == "GREEN")
        {
            currentColor = SupportedColors.GREEN;
        }
        else if (newColor == "BLUE")
        {
            currentColor = SupportedColors.BLUE;

        }
        else if (newColor == "LIGHTBLUE")
        {
            currentColor = SupportedColors.LIGHTBLUE;
        }  
        else if (newColor == "LIGHTGREEN") {
            currentColor = SupportedColors.LIGHTGREEN;
        }
        else if (newColor == "YELLOW")
        {
            currentColor = SupportedColors.YELLOW;
        }
        else if (newColor == "PINK")
        {
            currentColor = SupportedColors.PINK;
        }
        else if (newColor == "PURPLE")
        {
            currentColor = SupportedColors.PURPLE;
        }
        else if (newColor == "BROWN")
        {
            currentColor = SupportedColors.BROWN;
        }
        else if (newColor == "DARKGREEN")
        {
            currentColor = SupportedColors.DARKGREEN;
        }
        else if (newColor == "BLACK")
        {
            currentColor = SupportedColors.BLACK;
        }
        else
            Debug.Log("Unsupported Color Selected!!!");
    }

    #region UndoButton
    public void UpdateColorHistory()
    {
        Mesh roomMesh = mesh_handler.scene_mesh_object.GetComponent<MeshFilter>().mesh;
        colorHistory.AddLast(roomMesh.colors);
        create_label_script.SetUndoButtonActive(true);
        if (colorHistory.Count > 5)
        {
            colorHistory.RemoveFirst();
        }
    }

    public void UpdateMappingHistory()
    {
        mesh_handler.vertexSegmentIndicesHistory.AddLast(new List<int>(mesh_handler.vertexSegmentIndices));
        mesh_handler.segmentClassMappingHitory.AddLast(new Dictionary<int, Color>(mesh_handler.segmentClassMapping));
        mesh_handler.segmentVertexMappingHistory.AddLast(new Dictionary<int, List<int>>(mesh_handler.segmentVertexMapping));
        if (mesh_handler.vertexSegmentIndicesHistory.Count > 5)
        {
            mesh_handler.vertexSegmentIndicesHistory.RemoveFirst();
            mesh_handler.segmentClassMappingHitory.RemoveFirst();
            mesh_handler.segmentVertexMappingHistory.RemoveFirst();
        }
    }

    public void UndoStroke()
    {
        if (create_label_script.mode != "draw_face" && create_label_script.mode != "draw_seg")
            return;

        if (colorHistory.Count > 0)
        {
            // reset color
            Mesh roomMesh = mesh_handler.scene_mesh_object.GetComponent<MeshFilter>().mesh;
            roomMesh.colors = colorHistory.Last();
            colorHistory.RemoveLast();

            // reset mappings
            mesh_handler.vertexSegmentIndices = mesh_handler.vertexSegmentIndicesHistory.Last();
            mesh_handler.segmentClassMapping = mesh_handler.segmentClassMappingHitory.Last();
            mesh_handler.segmentVertexMapping = mesh_handler.segmentVertexMappingHistory.Last();
            mesh_handler.vertexSegmentIndicesHistory.RemoveLast();
            mesh_handler.segmentClassMappingHitory.RemoveLast();
            mesh_handler.segmentVertexMappingHistory.RemoveLast();
        }
        if (colorHistory.Count == 0)
        {
            create_label_script.SetUndoButtonActive(false);
        }
    }

    #endregion
}
