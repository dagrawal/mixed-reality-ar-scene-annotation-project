using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HololensConsole : MonoBehaviour
{
    [SerializeField]
    public static TextMeshProUGUI consoleText;

    public void Start()
    {
        consoleText = this.transform.Find("TextContent/TextMessage").GetComponent<TextMeshProUGUI>();
    }

    public static void DisplayMessage(string text)
    {
        if(consoleText != null)  
           consoleText.text = text;
    }
}
