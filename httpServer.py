from flask import Flask, request, jsonify
from SegmentationAlgorithms.segmentation import *
import socket
import json
import os
from io import StringIO

app = Flask(__name__)

"""
Reconstruct a mesh provided vertoces and triangles
"""

segments = {}


def reconstruct_mesh(data , mesh_path="mesh.obj" , vis_mesh=False):
    """
    Reconstruct a mesh provided vertices and triangles
    """
    json_data = json.loads(data)
    raw_label_data = json_data['label_data']
    print(raw_label_data)
    raw_dataframe = string_labels_to_df(raw_label_data)
    label_data = preprocess_dataframe(raw_dataframe)
    print("label_data", label_data)

    np_vertices = np.array(pd.DataFrame(json_data['vertices']))
    print("Number of vertices" , np_vertices.shape)
    np_triangles = np.array(pd.DataFrame(json_data['triangles']))
    np_triangles = np.reshape(np_triangles, (-1, 3))

    mesh = trimesh.Trimesh(vertices=np_vertices,
                           faces=np_triangles , process = False)

    print( len(np_vertices), len(mesh.vertices))
    trimesh.exchange.export.export_mesh(mesh, 'unity_reconstructed.obj')

    print("Done with reconstruction")

    return mesh, label_data

def convert_color_to_class(vertex_colors):
    
    vertex_classes = []

    for i in range(len(vertex_colors)):
        color = vertex_colors[i]
        current_class = ''
        if color[0] == 0 and color[1] == 0 and color[2] == 1:
            current_class = "FLOOR"
        elif color[0] == 1 and color[1] == 0 and color[2] == 0:
            current_class = "CEILING"
        elif color[0] == 0.1 and color[1] == 0.5 and color[2] == 0:
            current_class = "WALL"
        
        elif color[0] == 0 and color[1] == 1 and color[2] == 1:
            current_class = "COUCH"
        elif color[0] == 0 and color[1] == 1 and color[2] == 0:
            current_class = "CHAIR"
        elif color[0] == 1 and color[1] == 0.7 and color[2] == 0:
            current_class = "TABLE"
        elif color[0] == 1 and color[1] == 0 and color[2] == 1:
            current_class = "COUNTER"
        
        elif color[0] == 0.5 and color[1] == 0 and color[2] == 0.5:
            current_class = "REFRIGERATOR"
        elif color[0] == 0.5 and color[1] == 0.4 and color[2] == 0.0:
            current_class = "OVEN"
        elif color[0] == 0 and color[1] == 0.5 and color[2] == 0.4:
            current_class = "CABINET"
        elif color[0] == 0 and color[1] == 0 and color[2] == 0:
            current_class = "OTHER"    
        else:
            vertex_colors[i][0] = 0.5
            vertex_colors[i][1] = 0.5
            vertex_colors[i][2] = 0.5
            current_class = "UNLABLLED"

        vertex_classes.append(current_class)

    vertex_classes = np.asarray(vertex_classes)
    return vertex_colors , vertex_classes

def preprocess_description_string(description_string):
    """
    :param df: input string
    :return: cleaned dataframe containing only the points, labels and descriptions
    """
    description_string = description_string.replace(',', ';')
    DATA= StringIO(description_string)
    df = pd.read_csv(DATA, sep=";|:", header = None)

    df.columns = ['indexHeader', 'Index', 'positionHeader', 'x', 'y', 'z', 'descriptionHeader',
                  'Description']

    df = df.replace(['\(', '\)'], ['', ''], regex=True)
    df.x = df.x.astype('float64')
    df.y = df.y.astype('float64')
    df.z = df.z.astype('float64')
    df.Description = df.Description.astype('string')

    description_dataframe = df.drop(labels=['indexHeader', 'positionHeader', 'descriptionHeader'], axis=1)
    return description_dataframe

def reconstruct_segmented_mesh(data, mesh_path="mesh.obj", csv_path="segmentation_data"):
    # preprocess input string
    TESTDATA = data.replace("\n", "\\n")
    json_data = json.loads(TESTDATA)
    vertices = np.asarray(pd.DataFrame(json_data["mesh"]["vertices"]))
    triangles = np.asarray(json_data["mesh"]["triangles"]).reshape([-1,3])
    vertex_segment_indices = json_data["vertexSegmentIndices"]
    segment_class_mapping = json_data["segmentClassMapping"]
    description_string = json_data["descriptionString"]

    # process mesh
    segment_class_mapping['0'] = {'r':1.0 , 'g': 1.0 , 'b':1.0}
    vertex_colors = []
    for i in range(len(vertices)):
        color = segment_class_mapping[str(vertex_segment_indices[i])]
        vertex_colors.append([color['r'] , color['g'], color['b']])

    vertex_colors = np.asarray(vertex_colors)
    vertex_colors, vertex_classes = convert_color_to_class(vertex_colors)
    vertex_colors = 255*vertex_colors

    mesh = trimesh.Trimesh(vertices=vertices, faces=triangles , process=False , vertex_colors=vertex_colors)
    trimesh.exchange.export.export_mesh(mesh, mesh_path)

    vertex_indices = np.array([i for i in range(0, mesh.vertices.shape[0])])

    ## output vertex-to-segment mapping
    pd.DataFrame([vertex_indices, vertices[:,0] ,vertices[:,1],vertices[:,2], np.array(vertex_segment_indices), vertex_classes] ).T.to_csv(f"{csv_path}_vertex_segments.csv", header = ["index", "x","y" ,"z" , "subsegment_id", "class"])

    if len(description_string) != 0:
    	# Find label for each provided description tag
	    description_df = preprocess_description_string(description_string)
	    points = np.transpose(np.array([description_df['x'], description_df['y'], description_df['z']]))

	    # Find closest vertex for the description points
	    _, closest_vertex_list = trimesh.proximity.ProximityQuery(mesh).vertex(points)

	    # find segments and labels of the closest vertex
	    descriptions_to_segments = np.array(vertex_segment_indices)[closest_vertex_list]
	    descriptions_to_classes = np.array(vertex_classes)[closest_vertex_list]

	    ## output (segment index-class name-description)
	    pd.DataFrame([descriptions_to_segments, descriptions_to_classes, description_df['Description']]).T.to_csv(f"{csv_path}_descriptions.csv", header=["subsegment_id", "class", "description"])

    # ## output segment ID to class mapping
    # # colors per segment
    # segment_colors = []
    # segment_index = []
    # for entry in segment_class_mapping:
    #     r, g, b = segment_class_mapping[entry]['r'], segment_class_mapping[entry]['g'], segment_class_mapping[entry][
    #         'b']
    #     segment_colors.append([r, g, b])
    #     segment_index.append(entry)
    #
    # segment_colors, segment_classes = convert_color_to_class(segment_colors)
    #
    # pd.DataFrame([np.array(segment_index), segment_classes]).T.to_csv(f"{csv_path}_segment_classes.csv", header = ["segment_id" , "class"])

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"


@app.route("/upload", methods=['POST'])
def upload():
    # print(request.form)
    # print(request.data)
    msg_type = request.headers['Message-Type']
    # Assumes that segmentation and labels are sent (even if labels are empty)
    if msg_type == 'segmentation':
        print('Segmenting Mesh...')
        global segments
        segments = {}

        mesh, label_data = reconstruct_mesh(list(request.form.to_dict().keys())[0])
        segments = segment_and_generate_labels(mesh, label_data, store_segmented_mesh=True)

        return str(segments)

    elif msg_type == 'meshexport':
        print('Exporting Mesh...')
        os.makedirs('export', exist_ok=True)
        reconstruct_segmented_mesh(list(request.form.to_dict().keys())[0], 'export/scene.obj','export/labels')
        return "Success"

    elif msg_type == 'fulllabelexport':
        msg_list = list(request.form.to_dict().keys())
        if len(msg_list) > 0:
            os.makedirs('export', exist_ok=True)
            with open("export/labels_full.txt", "w") as text_file:
                text_file.write(msg_list[0])
            return "Success"

        else:
            print('No Labels to export.')
            return "Failed"

    elif msg_type == 'xyzlabelexport':
        msg_list = list(request.form.to_dict().keys())
        if len(msg_list) > 0:
            os.makedirs('export', exist_ok=True)
            with open("export/labels_points.txt", "w") as text_file:
                text_file.write(msg_list[0])
            return "Success"
        else:
            print('No Labels to export.')
            return "Failed"

    return "upload"


@app.route("/download", methods=['GET'])
def download():
    print("get has been called!")


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


if __name__ == '__main__':
    ip = get_ip()
    print('ip = {}'.format(ip))
    app.run(debug=True, host=ip)
